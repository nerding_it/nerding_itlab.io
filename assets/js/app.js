function ajax(method, url, data, success, error) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url);
  xhr.setRequestHeader("Accept", "application/json");
  xhr.onreadystatechange = function() {
    if (xhr.readyState !== XMLHttpRequest.DONE) return;
    if (xhr.status === 200) {
      success(xhr.response, xhr.responseType);
    } else {
      error(xhr.status, xhr.response, xhr.responseType);
    }
  };
  xhr.send(data);
}

function subscribeToNewsLetter(event) {
  event.preventDefault();
  function success () {
    document.getElementById('subscription-form').reset();
    const ele = document.getElementById('subscription-message');
    ele.style.display = 'block';
    setTimeout(() => {
      document.getElementById('subscription-message').style.display = 'none';
    }, 5000);
    if (ele.classList.contains('alert-danger')) {
      ele.classList.remove('alert-danger');
    }
    ele.classList.add('alert-success');
    ele.innerHTML = 'Thank you for subscribing!';
  }
  function error () {
    const ele = document.getElementById('subscription-message');
    ele.style.display = 'block';
    setTimeout(() => {
      document.getElementById('subscription-message').style.display = 'none';
    }, 5000);
    if (ele.classList.contains('alert-success')) {
      ele.classList.remove('alert-success');
    }
    ele.classList.add('alert-danger');
    ele.innerHTML = 'Oops! There was problem.';
  }
  const ele = document.getElementById('subscription-message');
  ele.style.display = 'block';
  if (ele.classList.contains('alert-danger')) {
    ele.classList.remove('alert-danger');
  }
  let form = document.getElementById('subscription-form');
  let email = document.getElementById('subscription-email');
  let formData = new FormData();
  formData.append('email', email.value);
  ajax(form.method, form.action, formData, success, error);
}

window.addEventListener('DOMContentLoaded', () => {
  document.getElementById('subscription-form').addEventListener('submit', subscribeToNewsLetter);
  (() => {
    var disqus_config = function () {
      this.page.url = document.baseURI;
      this.page.identifier = document.baseURI;
    };  
    var script = document.createElement('script');
    script.src = 'https://nerding-it.disqus.com/embed.js';
    script.setAttribute('data-timestamp', +new Date());
    document.head.appendChild(script);  
  })();

  (() => {
    setTimeout(() => {
      document.getElementsByClassName('content-wrapper')[0].style.marginTop = document.getElementById('navbar').offsetHeight + 'px';    
    }, 500);  
  })();
  if (window.location.pathname === '/') { 
    document.title = 'Sriharsha - Personal Website';
  }
});
